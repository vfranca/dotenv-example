<?php
use Dotenv\Dotenv;

require_once __DIR__.'/vendor/autoload.php';
$env = new Dotenv(__DIR__);
$env->load();

