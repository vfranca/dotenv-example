# Dotenv Example

Exemplo simples de implementação do arquivo .env

## Uso

```bash
# Instala as dependências do PHP
composer install
# Copia o arquivo .env.dist
cp .env.dist .env
# Executa index.php
php index.php
```

